import * as React from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';

const buttons = [
    <Button key="one">One</Button>,
    <Button key="two">Two</Button>,
    <Button key="three">Three</Button>,
];

export const ButtonTab = ({ size }) => {
    return (
        <ButtonGroup size={size} aria-label="small button group">
            {buttons}
        </ButtonGroup>
    );
};
