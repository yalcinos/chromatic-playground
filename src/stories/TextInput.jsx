import React from 'react';
import TextField from '@mui/material/TextField';

/**
 * Primary UI component for user interaction
 */
export const TextInput = ({ primary, label }) => {
    return (
        <TextField
            id="outlined-basic"
            label="Outlined"
            variant={label}
        />
    );
};
